var GAME = null;
const config = {
    width: 800,
    height: 600,
    type: Phaser.AUTO,
    parent: 'phaser-example',
    scene: {
        create,
        update,
    },
};
const game = new Phaser.Game(config);

// init = () => {
//     GAME.add.graphics({ lineStyle: { width: 2, color: 0xaa0000 }, fillStyle: { color: 0x0000aa } });
// };

function create() {
    GAME = this;

    this.player = new Phaser.Geom.Rectangle(50, 50, 50, 50);
    this.graphics = this.add.graphics({ lineStyle: { width: 2, color: 0xaa0000 }, fillStyle: { color: 0x0000aa } });

    // var graphics = this.add.graphics({ lineStyle: { width: 2, color: 0xaa0000 }, fillStyle: { color: 0x0000aa } });

    // var rect = new Phaser.Geom.Rectangle();

    // var square = new Phaser.Geom.Rectangle();


    // const keySpace = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
    // console.log(keySpace);
    // this.input.on('pointermove', function (pointer) {

    //     graphics.clear();

    //     rect.width = pointer.x;
    //     rect.height = pointer.y;

    //     var area = Phaser.Geom.Rectangle.Area(rect);

    //     square.width = square.height = Math.sqrt(area);

    //     graphics.fillRectShape(square);

    //     graphics.strokeRectShape(rect);

    // });
}

function update() {
    this.graphics.clear();
    this.graphics.fillRectShape(this.player);
}